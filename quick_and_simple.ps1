$total = 0
foreach( $digit in 1..999 )
{
    if( $digit % 3 -eq 0 )
    {
        $total += $digit
        continue
    }

    if( $digit % 5 -eq 0 )
    {
        $total += $digit
        continue
    }
}

$total