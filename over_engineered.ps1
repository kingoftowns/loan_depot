function Get-SumOfMultiples
{
    [cmdletbinding()]
    param(
        [Parameter( Mandatory=$true )]
        [ValidateScript( { $_ -gt 0 } )]
        [int[]]$MultiplesOf,
        [Parameter( Mandatory=$true )]
        [ValidateScript( { $_ -gt 0 } )]
        [int]$StartingWith,
        [Parameter( Mandatory=$true )]
        [ValidateScript( { $_ -gt 0 } )]
        [int]$UpTo
    )

    Begin
    {
        $list_of_multiples = New-Object System.Collections.Generic.List[System.Object]
        $sum_of_multiples = 0
    }
    
    Process
    {
        if( $UpTo -lt $StartingWith )
        {
            Throw "'UpTo' variable must be greater than 'StartingWith' variable"
        }
    
        # Loop through range, checking if $digit is multiple of each integer given
        foreach( $digit in ( $StartingWith )..( $UpTo ) )
        {
            $i = 0
            Do
            {
                if( $digit % $MultiplesOf[$i] -eq 0 )
                {
                    <#
                      if more than one int is supplied for MultiplesOf
                      make sure we don't add duplicates to our 
                      list of multiples
                    #>
                    if( $list_of_multiples -notcontains $digit )
                    {
                        $list_of_multiples.Add( $digit )
                    }
                }
                $i++
            } while( $i -lt $MultiplesOf.Count )
        }

        # Paranoid double checking for duplicates don't really need this next line
        $list_of_multiples = $list_of_multiples | Sort-Object | Get-Unique
    
        # Get sum of all integers returned 
        foreach( $number in $list_of_multiples )
        {
            $sum_of_multiples += $number
        }
    }

    End
    {
        return $sum_of_multiples
        $list_of_multiples,$sum_of_multiples | Clear-Variable 
        $list_of_multiples,$sum_of_multiples | Remove-Variable
    }
}

Get-SumOfMultiples -MultiplesOf 3,5 -StartingWith 1 -UpTo 999